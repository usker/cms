// 退出登录
const logout= document.querySelector('.logout');
logout.addEventListener('click',function(e){
    if(confirm('是否退出登录')) {
        localStorage.removeItem('token')
        location.href='./login.html'
    }
})

// 一级菜单点击
const liList= document.querySelectorAll('ul>li>a');
liList.forEach(loginA=>{
    loginA.addEventListener('click',function(e){
        // loginA.nextElementSibling.classList.toggle('show')
    // jquery 上下级缓慢显示
    $(loginA.nextElementSibling).slideToggle()
    })
})

// 二级点击切换高亮
const subList= document.querySelectorAll('.nav ul a');
subList.forEach(subA=>{
    subA.addEventListener('click',function(e){
        document.querySelector('.nav ul a.active').classList.remove('active')
        subA.classList.add('active') 
    })  
})

// 初始化数据
const init = document.querySelector('.init');
init.addEventListener('click',async function(e){
    const result = await axios({
        url:'/init/data'         
    })
    console.log(result);
})