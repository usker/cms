// 查询所有成员信息
const tbody = document.querySelector("tbody");
async function getScore() {
  let html = "";
  const result = await axios({
    url: "/score/list",
  });
  const data = result.data.data;
  // console.log(data);
  for (const key in data) {
    html += `
            <tr>
                <th scope="row">${key}</th>
                <td>${data[key].name}</td>
                <td class="score" data-id='${key}' data-batch='${1}'>${
      data[key].score[0]
    }</td>
                <td class="score" data-id='${key}' data-batch='${2}'>${
      data[key].score[1]
    }</td>
                <td class="score" data-id='${key}' data-batch='${3}'>${
      data[key].score[2]
    }</td>
                <td class="score" data-id='${key}' data-batch='${4}'>${
      data[key].score[3]
    }</td>
                <td class="score" data-id='${key}' data-batch='${5}'>${
      data[key].score[4]
    }</td>
              </tr>`;
  }
  tbody.innerHTML = html;
}
getScore();

// 双击修改成绩
// 事件委托来点击
tbody.addEventListener("dblclick", function (e) {
  if (e.target.classList.contains("score")) {
    const input = document.createElement("input");
    input.style = "text";
    e.target.append(input);
    input.style.display = "block";
    // 重新添加旧的值
    input.value = e.target.innerText;
    input.focus();
    // 按下回车新增值
    input.addEventListener("keyup", function (ev) {
      if (ev.key === "Enter") {
        eventFunc();
        // 事件解绑
        input.removeEventListener("blur", eventFunc);
      }
    });
    // 失去焦点
    input.addEventListener("blur", eventFunc);
    // 按下回车和失去焦点 事件处理函数
    async function eventFunc() {
      const { id, batch } = e.target.dataset;
      const result = await axios({
        url: "/score/entry",
        method: "post",
        data: {
          stu_id: id,
          batch: batch,
          score: input.value,
        },
      });
      console.log(result);
      if (result.data.code === 0) {
        toastr.success(result.data.message);
        //刷新页面数据
        getScore();
      }
      console.log(result.data.code);
    }
  }
});
