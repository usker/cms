// axios 基地址
axios.defaults.baseURL = "http://www.itcbc.com:8000/";
// 显示自动加载和关闭加载
// 错误提示
axios.interceptors.request.use(
    function (config) {
      NProgress.start(); // 显示加载中
      // 拦截器携带token
      if(localStorage.getItem('token')) {
        config.headers.Authorization=localStorage.getItem('token')
      }
      return config;
    },
    function (error) {
      return Promise.reject(error);
    }
  );
  
  axios.interceptors.response.use(
    function (response) {
      NProgress.done(); // 关闭加载中
      if(response.data.code!==undefined&&response.data.code!==0) {
        toastr.error(response.data.message)
      }
      return response;
    },
    function (error) {
      NProgress.done(); // 关闭加载中
      toastr.error(error.response.data.message)
      return Promise.reject(error);
    }
  );
  