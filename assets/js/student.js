const tbody = document.querySelector("tbody");
const showModalBtn = document.querySelector(".show-modal-btn"); //添加学员 显示模态框
const provinceDom = document.querySelector("[name=province");
const cityDom = document.querySelector("[name=city]");
const countyDom = document.querySelector("[name=county]");
const addForm = document.querySelector(".add-form"); //添加表单
const addModalLabel = document.querySelector("#addModalLabel"); //添加表单标题
//初始化工作
function init() {
  getStudent();
  getProvince();
  //省份绑定change事件
  provinceDom.addEventListener("change", function () {
    getCity(provinceDom.value);
  });

  //市绑定change事件
  cityDom.addEventListener("change", function () {
    getCounty(provinceDom.value, cityDom.value);
  });
}
init();

// 查询学员信息并渲染表格
async function getStudent() {
  const result = await axios({
    url: "/student/list",
  });
  const list = result.data.data;
  // console.log(list);
  const newList = list.map((item) => {
    const {
      id,
      name,
      age,
      sex,
      group,
      phone,
      salary,
      truesalary,
      province,
      city,
      county,
    } = item;
    return `<tr>
        <th scope="row">${id}</th>
        <td>${name}</td>
        <td>${age}</td>
        <td>${sex}</td>
        <td>${group}</td>
        <td>${phone}</td>
        <td>${truesalary}</td>
        <td>${salary}</td>
        <td>${province + city + county}</td>
        <td>
          <button type="button" class="btn btn-primary btn-sm btn-update" data-id='${id}'>修改</button>
          <button type="button" class="btn btn-danger btn-sm">删除</button>
        </td>
      </tr>`;
  });
  tbody.innerHTML = newList.join("");
}

// 添加模态框
showModalBtn.addEventListener("click", function (e) {
  $("#addModal").modal("show");
  addModalLabel.innerText = "新增学员信息";
});

// 添加表单提交事件
addForm.addEventListener("submit", async function (e) {
  e.preventDefault();
  const data = serialize(addForm, { hash: true, empty: true });
  console.log(data);
  let result;
  // 判断是新增还是编辑
  if (addModalLabel.innerText === "新增学员信息") {
    result = await axios({ url: "/student/add", method: "post", data });
  } else {
    // 获取编辑数据ID
    data.id = id;
    result = await axios({ url: "/student/update", method: "put", data });
  }
  if (result.data.code === 0) {
    toastr.success("操作成功");
    addForm.reset();
    $("#addModal").modal("hide");
    // 刷新数据
    getStudent();
  }
});

// 修改  ========>懵逼
tbody.addEventListener("click", async function (e) {
  if (e.target.classList.contains("btn-update")) {
    $("#addModal").modal("show");
    addModalLabel.innerText = "编辑学员信息";
    // 获取id
    id = e.target.dataset.id;
    const result = await axios({ url: "/student/one", params: { id } });
    const data = result.data.data;
    // 数据回填功能
    await getCity(data.province); //异步
    await getCounty(data.province, data.city);
    // 获取addForm标签里的所有带name属性的dom元素
    const nameDomList = addForm.querySelectorAll("[name]");
    nameDomList.forEach((dom) => {
      if (dom.name !== "sex") {
        addForm.querySelector(`[name="${dom.name}"]`).value = data[dom.name];
      }
    });
    addForm.querySelector(`[name=sex][value=${data.sex}]`).checked = true;
  }
});

// 获取省份
async function getProvince() {
  const result = await axios({
    url: "/geo/province",
  });
  const data = result.data;
  //   console.log(data);
  const newArr = data.reduce(
    (prev, item) => prev + `<option value="${item}">${item}</option>`,
    '<option value="">--省--</option>'
  );
  provinceDom.innerHTML = newArr;
}

// 获取市区
async function getCity(pname) {
  const result = await axios({
    url: "/geo/city",
    params: { pname },
  });
  cityDom.innerHTML = result.data.reduce(
    (prev, item) => prev + `<option value="${item}">${item}</option>`,
    '<option value="">--市--</option>'
  );
  // 还原 县
  // cityDom.innerHTML='<option value="">--县--</option>'
}

// 获取地区
async function getCounty(pname, cname) {
  const result = await axios({
    url: "/geo/county",
    params: { pname, cname },
  });
  countyDom.innerHTML = result.data.reduce(
    (prev, item) => prev + `<option value="${item}">${item}</option>`,
    '<option value="">--县--</option>'
  );
}
