const loginA= document.querySelector('.login-a');
const registerA= document.querySelector('.register-a');

// 获取登入/注册盒子
const register= document.querySelector('.register');
const login= document.querySelector('.login');

// 获取登入/注册表单
const loginForm= document.querySelector('.login form');
const registerForm= document.querySelector('.register form');

// 登入/注册切换
loginA.addEventListener('click',function(e){
    login.style.display='none'
    register.style.display='block'
})
registerA.addEventListener('click',function(e){
    login.style.display='block'
    register.style.display='none'
})

registerForm.addEventListener('submit',async function(e){
    e.preventDefault()
    const data=serialize(registerForm,{hash:true,empty:true})
    console.log(data);
    const result = await axios({
        url:'/api/register',
        method:'post',
        data
    })
    console.log(result);
    if(result.data.code ===0) {
        toastr.success("注册成功");
        registerForm.reset()
        registerA.click()
        
    } 
})

loginForm.addEventListener('submit',async function(e){
    e.preventDefault()
    const data=serialize(loginForm,{hash:true,empty:true})
    const result = await axios({
        url:'/api/login',
        method:'post',
        data
    })
    if(result.data.code===0) {
        toastr.success(result.data.message)
        // 判断有没有token
        // console.log(result.data.token);
        localStorage.setItem('token',result.data.token)
        loginForm.reset()
        // loginA.click()
        location.href='./index.html'
    }  
})